﻿using Soln_SalesTableInlineExpression.Dal.ORM;
using Soln_SalesTableInlineExpression.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Soln_SalesTableInlineExpression.Dal
{
    public class SalesDal
    {
         #region Declaration
        private SalestabledcDataContext  dc = null;
        #endregion

        #region ConStructor
        public SalesDal()
        {
            dc = new SalestabledcDataContext();
        }
        #endregion

        public IEnumerable<SalesEntity> GetSalesData()
        {
            var getQuery =
                    dc
                    .SalesOrderDetails
                    .AsEnumerable()
                    .Select((leLinqSalesObj) => new SalesEntity()
                    {
                        Salesorderid = leLinqSalesObj.SalesOrderID,
                        Orderqty = leLinqSalesObj.OrderQty,
                        Productid = leLinqSalesObj.ProductID,
                        Unitprice = leLinqSalesObj.UnitPrice
                    })
                    .Take(5)
                    .ToList();

            return getQuery;
        }
    }
}