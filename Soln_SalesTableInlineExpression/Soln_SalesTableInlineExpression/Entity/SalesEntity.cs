﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Soln_SalesTableInlineExpression.Entity
{
    public class SalesEntity
    {
        public dynamic Salesorderid { get; set; }
        public dynamic Orderqty { get; set; }
        public dynamic Productid { get; set; }
        public dynamic Unitprice { get; set; }

    }
}