﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="Soln_SalesTableInlineExpression.WebForm1" %>
<%@import Namespace ="Soln_SalesTableInlineExpression.Dal "%>
<%@import Namespace ="Soln_SalesTableInlineExpression.Entity "%>

<!DOCTYPE html>
<style type="text/css">
    table{
            width:50%;
            margin:auto;
             border-collapse:collapse;
        }

        th,td {
            border:1px;
            border-style:solid;
            border-color:gray;
            text-align:center;
            padding:20px;
            margin:10px;
        }

        tr:nth-child(even){
            background-color:gray;
            color:white;
        }
</style>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table>
               <tr>
                   <th> Salesorderid</th>
                   <th>Orderqty</th>
                   <th>Productid</th>
                   <th>Unitprice</th>
               </tr>

               <%
                   // get Data from Person Dal
                   var selectSalesData = new SalesDal().GetSalesData();

                   %>

               <%
                    // Foreeach Start.
                   foreach (var Obj in selectSalesData)
                   {
                   %>
                        <tr>
                            <td>
                                <span>
                                   
                                   <%: Server.HtmlEncode(Obj.Salesorderid) %>
                             
                                       </span>
                            </td>
                           <td>
                               <span>

                                   <% Response.Write(Server.HtmlEncode(Obj.Orderqty)); %> 


                               </span>
                           </td>
                           <td>
                               <span>
                                 
                                     <% Response.Write(Server.HtmlEncode(Obj.Productid)); %>
                               
                               </span>
                            </td>
                            <td>
                               <span>
                                 
                                     <% Response.Write(Server.HtmlEncode(Obj.Unitprice)); %>
                               
                               </span>
                           </td>
                           
                       </tr>
               <%
                   } 
                   %>

           </table>

    </div>
    </form>
</body>
</html>
